/*
 *     MainActivity.kt
 *     Gugal
 *     Copyright (c) 2021 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.*
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Scaffold
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import androidx.core.view.WindowCompat
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.porg.gugal.Global.Companion.pastSearches
import com.porg.gugal.Global.Companion.serpProvider
import com.porg.gugal.Global.Companion.sharedPreferences
import com.porg.gugal.news.NewsPage
import com.porg.gugal.providers.exceptions.InvalidCredentialException
import com.porg.gugal.setup.SetupStartActivity
import com.porg.gugal.ui.theme.GugalTheme

@ExperimentalAnimationApi
@ExperimentalMaterialApi
class MainActivity : ComponentActivity() {

    private var apikey = ""
    private var cx = ""
    private var ca: List<String>? = null
    private lateinit var context: Context
    private var invalidCredentials: Boolean = false

    var showLoadOldPrefsAlert = false
    private var showSetup = false

    @ExperimentalAnimationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)

        // Create global sharedPreferences object
        Global.createSharedPreferences(applicationContext)

        loadSerp()
        Log.d("gugal", "show setup: $showSetup")
        if (showSetup) {
            showSetup = false
            val intent = Intent(applicationContext, SetupStartActivity::class.java)
            intent.component =
                ComponentName("com.porg.gugal", "com.porg.gugal.setup.SetupStartActivity")
            startActivity(intent)
            return
        }

        ca = loadCxApi()
        if (serpProvider.id.endsWith("-goog") && (ca?.size ?: 0) == 2) {
            if (ca?.get(0) ?: "none" != "none") cx = ca?.get(0) ?: ""
            if (ca?.get(1) ?: "none" != "none") apikey = ca?.get(1) ?: ""
        }

        context = this

        val simple = resources.getBoolean(R.bool.isSimple);

        setContent {
            GugalTheme {
                if (showLoadOldPrefsAlert) {
                    Column(modifier = Modifier.fillMaxWidth().fillMaxHeight(),
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally) {
                        Text("Updating save data...", Modifier.padding(bottom = 16.dp))
                        LinearProgressIndicator()
                    }
                    resaveGoogleNonSerpPrefs(cx, apikey)
                } else {
                    // A surface container using the 'background' color from the theme
                    Surface(
                        color = MaterialTheme.colorScheme.background,
                        modifier = Modifier.statusBarsPadding()
                    ) {
                        if (simple) ResultPage(context, ca.isNullOrEmpty() || invalidCredentials)
                        else {
                            val navController = rememberNavController()
                            Scaffold(
                                content = { padding ->
                                    NavigationHost(
                                        navController = navController,
                                        padding = padding
                                    )
                                },
                                bottomBar = { BottomNavigationBar(navController = navController) },
                                backgroundColor = Color.Transparent
                            )
                        }
                    }
                }
            }
        }
    }

    // Loads the SERP provider.
    private fun loadSerp() {
        val serpID = sharedPreferences.getString("serp", "none")
        Log.d("gugal", "loadSerp: serp ID is $serpID")
        pastSearches = sharedPreferences.getStringSet("pastSearches", pastSearches) as MutableSet<String>
        if (serpID != "none") Global.setSerpProvider(serpID!!)
        else showSetup = true
    }

    private fun resaveGoogleNonSerpPrefs(cx: String, apikey: String) {
        with (sharedPreferences.edit()) {
            // Edit the user's shared preferences...
            this.putString("serp_${serpProvider.id}_cx", cx)
            this.putString("serp_${serpProvider.id}_ak", apikey)
            // ...and remove the old preferences
            this.remove("serp_google_data_cx")
            this.remove("serp_google_data_ak")
            apply()
        }
        showLoadOldPrefsAlert = false
    }

    private fun loadCxApi(): List<String>? {
        val arr: ArrayList<String> = ArrayList()
        // load pre-0.4 prefs if the SERP provider is google and they exist
        if (sharedPreferences.getString("serp_google_data_cx", "none") != "none" && serpProvider.id.endsWith("-goog")) {
            sharedPreferences.getString("serp_google_data_cx", "none")?.let { arr.add(it) }
            sharedPreferences.getString("serp_google_data_ak", "none")?.let { arr.add(it) }
            showLoadOldPrefsAlert = true;
            return (arr.toArray() as? Array<*>)?.filterIsInstance<String>()
        }

        val crdMap: MutableMap<String, String> = mutableMapOf()
        for (name in serpProvider.getSensitiveCredentialNames())
            sharedPreferences.getString("serp_${serpProvider.id}_${name}", "none")?.let { if (it != "none") {arr.add(it); crdMap[name] = it} }
        try {
            serpProvider.useSensitiveCredentials(crdMap)
        } catch (x: InvalidCredentialException) {
            Log.w("Gugal", "Received invalid credentials, should re-run setup.")
            invalidCredentials = true
        }
        return (arr.toArray() as? Array<*>)?.filterIsInstance<String>()
    }

    @Composable
    fun BottomNavigationBar(navController: NavHostController) {

        NavigationBar {
            val backStackEntry by navController.currentBackStackEntryAsState()
            val currentRoute = backStackEntry?.destination?.route

            var navItems = NavBarItems.BarItems
            if (Global.gugalNews) navItems = NavBarItems.BarItemsWithNews

            navItems.forEach { navItem ->

                NavigationBarItem(
                    selected = currentRoute == navItem.route,
                    onClick = {
                        navController.navigate(navItem.route) {
                            popUpTo(navController.graph.findStartDestination().id) {
                                saveState = true
                            }
                            launchSingleTop = true
                            restoreState = true
                        }
                    },

                    icon = {
                        Icon(imageVector = ImageVector.vectorResource(navItem.image),
                            contentDescription = getText(navItem.title).toString())
                    },
                    label = {
                        Text(text = getText(navItem.title).toString())
                    },
                )
            }
        }
    }

    @Composable
    fun NavigationHost(navController: NavHostController, padding: PaddingValues) {
        NavHost(
            navController = navController,
            startDestination = Routes.Search.route,
            modifier = Modifier.padding(padding)
        ) {
            composable(Routes.Search.route) {
                ResultPage(context, ca.isNullOrEmpty() || invalidCredentials)
            }

            composable(Routes.Settings.route) {
                SettingsPage(context)
            }

            composable(Routes.News.route) {
                NewsPage(context)
            }
        }
    }
}