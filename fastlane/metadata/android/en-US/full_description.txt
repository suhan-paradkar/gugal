Gugal is a clean and lightweight web search app made with Jetpack Compose. It has a modern, Material 3/You UI.

It supports 2 search engines - Google and searx/SearXNG. Developers are free to add more search engines.

To use Gugal with Google Search you need a CSE ID and API key. Instructions for obtaining both can be found in the app. <b>To get these you have to have a Google account, therefore Google might still link searches to your account. If you want to use a search engine that respects your privacy more, use Gugal with a searx/SearXNG instance.</b>

<i>Google and Google Search are trademarks of Google LLC.</i>