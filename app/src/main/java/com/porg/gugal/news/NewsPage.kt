/*
 *     NewsPage.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.news

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import com.porg.gugal.cardElevation
import com.porg.gugal.ui.theme.shapeScheme

@Composable
fun NewsPage(context: Context?) {
    val articles = List(5) { i ->
        Article(
            "Article ${i+1}",
            "This is a test article. Lorem ipsum dolor sit amet",
            "https://gitlab.com/narektor/gugal",
            "Gugal",
            "asite.com"
        )
    }
    Column {
        NewsHeader()
        Articles(articles, context = context)
    }
}

@Composable
fun NewsHeader() {
    Text(
        text = "Top stories",
        modifier = Modifier.padding(start = 14.dp, end = 14.dp, top = 40.dp),
        style = MaterialTheme.typography.displaySmall
    )
    Text(
        text = "Date here",
        modifier = Modifier.padding(start = 14.dp, end = 14.dp, bottom = 30.dp),
        style = MaterialTheme.typography.bodyLarge
    )
}

@Composable
fun ArticleCard(art: Article, context: Context?) {
    Surface(
        shape = MaterialTheme.shapeScheme.medium,
        tonalElevation = cardElevation,
        modifier = Modifier
            .padding(start = 14.dp, end = 14.dp, top = 4.dp, bottom = 8.dp)
            .fillMaxWidth()
            .clickable(onClick = {
                if (art.url != "") {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(art.url))
                    // Note the Chooser below. If no applications match,
                    // Android displays a system message.So here there is no need for try-catch.
                    ContextCompat.startActivity(
                        context!!,
                        Intent.createChooser(intent, "Open result in"),
                        null
                    )
                }
            }),
    ) {
        Column {
            Text(
                text = art.title,
                modifier = Modifier.padding(all = 14.dp),
                style = MaterialTheme.typography.titleLarge
            )
            Text(
                text = art.body,
                modifier = Modifier.padding(bottom = 14.dp, start = 14.dp, end = 14.dp),
                style = MaterialTheme.typography.bodyLarge
            )
            if (art.author != "") {
                Text(
                    text = "by " + art.author,
                    modifier = Modifier.padding(bottom = 2.dp, start = 14.dp, end = 14.dp),
                    style = MaterialTheme.typography.titleSmall
                )
            }
            // Add a vertical space between the author and message texts
            Spacer(modifier = Modifier.height(14.dp))
        }
    }
}

@Composable
fun Articles(articles: List<Article>, context: Context?) {
    LazyColumn {
        items(articles) { article ->
            ArticleCard(article, context)
        }
    }
}