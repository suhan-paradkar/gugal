/*
 *     DevOptExperimentActivity.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.devopt

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import com.porg.gugal.Global.Companion.gugalNews
import com.porg.gugal.Global.Companion.resAppAnim
import com.porg.gugal.MainActivity
import com.porg.gugal.Material3Settings
import com.porg.gugal.ui.theme.GugalTheme

class DevOptExperimentActivity : ComponentActivity() {
    @OptIn(
        androidx.compose.material3.ExperimentalMaterial3Api::class
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            GugalTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colorScheme.background) {
                    Column(
                        modifier = Modifier.fillMaxSize()
                    ) {
                        Material3Settings.RegularSetting(
                            title = "News",
                            body = "Toggles the News tab, featuring a clean news feed powered by RSS, for this session.",
                            onClick = {
                                gugalNews = !gugalNews
                                showToast(gugalNews, "News")
                            },
                        )
                        Material3Settings.RegularSetting(
                            title = "Result appearance animation",
                            body = "Toggles an animation for the results appearing. This is here because it hasn't been implemented well and should be optimized before it's officially enabled.",
                            onClick = {
                                resAppAnim = !resAppAnim
                                showToast(resAppAnim, "Appearance animation")
                            },
                        )
                    }
                }
            }
        }
    }

    @OptIn(ExperimentalAnimationApi::class, ExperimentalMaterialApi::class)
    private fun showToast(what: Boolean, name: String) {
        var ed = "en"; if (!what) ed = "dis"
        Toast.makeText(applicationContext, "$name has been ${ed}abled for this session.",
            Toast.LENGTH_SHORT).show()
        startActivity(Intent(applicationContext, MainActivity::class.java))
    }
}