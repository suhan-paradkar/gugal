/*
 *     Global.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.porg.gugal.devopt.providers.AlwaysFailingProvider
import com.porg.gugal.providers.SerpProvider
import com.porg.gugal.providers.cse.GoogleCseSerp
import com.porg.gugal.providers.searx.SearXSerp

class Global {
    companion object {
        var pastSearches: MutableSet<String> = mutableSetOf()
        lateinit var serpProvider: SerpProvider
        // TODO if adding SERP providers, add your provider in the function and array below
        val allSerpProviders: Array<String> = arrayOf(
            GoogleCseSerp.id,
            SearXSerp.id
        )
        fun setSerpProvider(serpID: String) {
            if (serpID == GoogleCseSerp.id) serpProvider = GoogleCseSerp()
            else if (serpID == SearXSerp.id) serpProvider = SearXSerp()
            // Check if serpID matches your SERP provider's ID, and if so set Global.serpProvider to
            // an instance of your SERP provider.
            else if (serpID == AlwaysFailingProvider.id) serpProvider = AlwaysFailingProvider()
        }

        private val keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC
        private val mainKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec)
        const val sharedPrefsFile = "gugalprefs"
        lateinit var sharedPreferences: SharedPreferences

        fun createSharedPreferences(appContext: Context) {
            sharedPreferences = EncryptedSharedPreferences.create(
                sharedPrefsFile,
                mainKeyAlias,
                appContext,
                EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            )
        }

        var devSerp: Boolean = false
        var gugalNews: Boolean = false
        var resAppAnim: Boolean = false
    }
}