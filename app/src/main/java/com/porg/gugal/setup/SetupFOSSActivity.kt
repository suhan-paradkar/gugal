/*
 *     SetupFOSSActivity.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.setup

import android.content.ComponentName
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.porg.gugal.MainActivity
import com.porg.gugal.Material3SetupWizard
import com.porg.gugal.Material3SetupWizard.Companion.Tip
import com.porg.gugal.R
import com.porg.gugal.ui.theme.GugalTheme

class SetupFOSSActivity : ComponentActivity() {
    @OptIn(ExperimentalMaterialApi::class,
        ExperimentalAnimationApi::class, ExperimentalMaterial3Api::class
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val ctx = this
        setContent {
            val scrollBehavior = TopAppBarDefaults.exitUntilCollapsedScrollBehavior()
            GugalTheme {
                Scaffold(
                    modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
                    topBar = {
                        LargeTopAppBar(
                            title = {
                                Text(
                                    getText(R.string.setup_p4_title).toString(),
                                    maxLines = 1,
                                    overflow = TextOverflow.Ellipsis
                                )
                            },
                            navigationIcon = {
                                IconButton(onClick = { finish() }) {
                                    Icon(
                                        imageVector = Icons.Filled.ArrowBack,
                                        contentDescription = "Go back",
                                    )
                                }
                            },
                            scrollBehavior = scrollBehavior
                        )
                    },
                    content = { innerPadding ->
                        Surface(color = MaterialTheme.colorScheme.background) {
                            Column(modifier = Modifier.padding(innerPadding)
                                    .verticalScroll(rememberScrollState()).fillMaxSize()
                            ) {
                                Text(
                                    text = getText(R.string.setup_p4_description).toString(),
                                    modifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp, vertical = 8.dp),
                                    style = MaterialTheme.typography.bodyLarge
                                )
                                Tip(
                                    text = getText(R.string.setup_p4_tip_1).toString(),
                                    icon = R.drawable.ic_warning,
                                    modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
                                    onClick = {
                                        val intent = Intent(Intent.ACTION_VIEW,
                                            Uri.parse("https://gugal.gitlab.io/why-official.html"))
                                        // Note the Chooser below. If no applications match,
                                        // Android displays a system message.So here there is no need for try-catch.
                                        startActivity(Intent.createChooser(intent, "Open notice in"))
                                    }
                                )
                                Tip(
                                    text = getText(R.string.setup_p4_tip_2).toString(),
                                    modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
                                    image = R.drawable.ic_donate
                                )
                            }
                        }
                        Material3SetupWizard.TwoButtons(
                            positiveAction = {
                                val intent = Intent(ctx, MainActivity::class.java)
                                intent.component =
                                    ComponentName("com.porg.gugal", "com.porg.gugal.MainActivity")
                                startActivity(intent)
                            },
                            positiveText = getString(R.string.btn_next),
                            negativeAction = {
                                finish()
                            }
                        )
                    }
                )
            }
        }
    }
}