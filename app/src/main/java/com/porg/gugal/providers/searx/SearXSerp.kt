/*
 *     SearXSerp.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.providers.searx

import android.content.Context
import android.util.Log
import android.webkit.URLUtil
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import com.android.volley.NetworkError
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.porg.gugal.R
import com.porg.gugal.Result
import com.porg.gugal.providers.ProviderInfo
import com.porg.gugal.providers.SerpProvider
import com.porg.gugal.providers.exceptions.InvalidCredentialException
import com.porg.gugal.providers.responses.ErrorResponse
import com.porg.gugal.providers.responses.InvalidCredentialResponse
import org.json.JSONObject
import java.net.URI
import java.net.URL

class SearXSerp: SerpProvider {

    private var url = ""

    private val connStateDefault = 0
    private val connStateFail = 1
    private val connStatePass = 2
    private val connStateWait = 3

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    override fun ConfigComposable(
        modifier: Modifier,
        enableNextButton: MutableState<Boolean>,
        context: Context
    ) {
        val _url = remember { mutableStateOf(TextFieldValue()) }
        val _connTest = remember { mutableStateOf(connStateDefault) }
        Column(
            modifier = modifier
        ) {
            TextField(
                placeholder = { Text(text = stringResource(R.string.serp_searx_instance)) },
                value = _url.value,
                modifier = Modifier
                    .padding(all = 4.dp)
                    .fillMaxWidth(),
                onValueChange = { nv ->
                    _url.value = nv
                    url = _url.value.text
                    enableNextButton.value = URLUtil.isValidUrl(url)
                },
                isError = (_connTest.value == connStateFail),
                keyboardActions = KeyboardActions(
                    onDone = {
                        url = _url.value.text
                        enableNextButton.value = URLUtil.isValidUrl(url)
                    }
                ),
                maxLines = 1,
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Done
                )
            )
            Text(
                text = stringResource(R.string.serp_searx_apiAccess),
                modifier = Modifier.padding(all = 4.dp),
                style = MaterialTheme.typography.bodyLarge
            )
            Button(
                onClick = {connTest(_connTest, context)},
                modifier = Modifier.padding(all = 4.dp).fillMaxWidth()
            ) {
                Text("Check support")
            }
            when (_connTest.value) {
                connStatePass -> Text("Supported instance")
                connStateFail -> Text("Unsupported instance")
                connStateWait -> LinearProgressIndicator(
                    modifier = Modifier.padding(all = 4.dp).fillMaxWidth()
                )
            }
        }
    }

    fun connTest(state: MutableState<Int>, context: Context) {
        // Create a JSON request, searching the instance for "gugal app"
        val jr: JsonObjectRequest = JsonObjectRequest(
            Request.Method.GET, makeURL("gugal+app"), null,
            { response ->
                var result = false
                // If there is a results key, check it it's empty
                if (response.has("results")) {
                    val items = response.getJSONArray("results")
                    result = items.length() > 0
                }
                // Otherwise set the result to false
                else result = false
                // Update mutable state
                if (!result) state.value = connStateFail
                else state.value = connStatePass
            },
            // Any HTTP errors fail the connectivity test
            // This is done because some instances block API access by making /search return HTTP 403
            { state.value = connStateFail }
        )
        val queue: RequestQueue = Volley.newRequestQueue(context)
        queue.add(jr)
        state.value = connStateWait
    }

    override fun getSensitiveCredentials(): Map<String, String> {
        return mapOf("url" to url)
    }

    override fun getSensitiveCredentialNames(): Array<String> {
        return arrayOf("url")
    }

    override fun useSensitiveCredentials(credentials: Map<String, String>) {
        if (!credentials.containsKey("url")) throw InvalidCredentialException()
        url = credentials["url"]!!
    }

    private fun makeURL(query: String): String {
        if (!url.contains("https")) url = "https://$url"
        val uri: URL = URI.create(url).toURL()
        return uri.protocol.toString() + "://" + uri.authority +
               "/?q=$query&format=json&lang=en&time_range=day"
    }

    override fun search(
        query: String,
        resultList: SnapshotStateList<Result>,
        isError: MutableState<ErrorResponse?>
    ): JsonObjectRequest? {
        // Request a string response from the provided URL.
        return JsonObjectRequest(
            Request.Method.GET, makeURL(query), null,
            { response ->
                val items = response.getJSONArray("results")
                resultList.clear()
                for (i in 0 until items.length()) {
                    val item: JSONObject = items.getJSONObject(i)
                    // get parsed URL
                    val pars = item.getJSONArray("parsed_url")
                    // show the engine name in the domain too
                    val domain = "${pars[1]} (via ${item.getString("engine")})"
                    if (item.has("content"))
                        resultList.add(Result(item.getString("title"), item.getString("content"),
                            item.getString("url"), domain))
                    else
                        resultList.add(Result(item.getString("title"), null,
                            item.getString("url"), domain))
                }
            },
            { error ->
                Log.e("SearXSerp", "Error!")
                if (error is NetworkError) {
                    error.message?.let {
                        // Detect invalid SearX URL
                        if (it.contains("UnknownHost")) isError.value = InvalidCredentialResponse()
                        else isError.value = ErrorResponse(it, "err")
                    }
                } else {
                    //val status = error.networkResponse.statusCode
                    // check for 4xx error codes, not all instances return 403
                    //if (status in 400..499) throw InvalidCredentialException()
                    error.message?.let {
                        val nr = error.networkResponse
                        if (nr == null)
                            isError.value = ErrorResponse(it, "err")
                        else
                            isError.value = ErrorResponse(it, nr.statusCode.toString())
                    }
                }
            }
        )
    }

    override val providerInfo = Companion.providerInfo
    override val id: String get() = Companion.id

    companion object {
        val id: String = "198debd01044412d8f2c22d9e2ae9e8d-searx"
        val providerInfo = ProviderInfo(
            R.string.serp_searx,
            R.string.serp_searx_desc,
            R.string.serp_searx,
            true)
    }
}