/*
 *     AlwaysFailingProvider.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.devopt.providers

import android.content.Context
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.android.volley.toolbox.JsonObjectRequest
import com.porg.gugal.R
import com.porg.gugal.Result
import com.porg.gugal.providers.ProviderInfo
import com.porg.gugal.providers.SerpProvider
import com.porg.gugal.providers.responses.ErrorResponse
import com.porg.gugal.providers.responses.InvalidCredentialResponse
import com.porg.gugal.providers.responses.NoResultsResponse

class AlwaysFailingProvider: SerpProvider {

    override val id: String get() = Companion.id
    override val providerInfo = ProviderInfo(
        R.string.app_name,
        R.string.app_name,
        R.string.app_name,
        false
    )

    companion object {
        val id: String = "debug-fails"
    }

    @Composable
    override fun ConfigComposable(
        modifier: Modifier,
        enableNextButton: MutableState<Boolean>,
        context: Context
    ) {
        Column(
            modifier = Modifier
                .padding(all = 4.dp)
                .then(modifier)
        ) {
            Text(
                text = "This SERP provider will always return an ErrorResponse.\n\n" +
                        "If \"invalid_credentials\" is searched, an invalid credential response is returned.\n\n" +
                        "If \"nothing\" is searched, a no results response is returned.\n\n" +
                        "If anything else is searched, a generic error response with a custom message is returned.",
                modifier = Modifier.padding(all = 4.dp),
                style = MaterialTheme.typography.bodyLarge
            )
        }
    }

    override fun search(
        query: String,
        resultList: SnapshotStateList<Result>,
        isError: MutableState<ErrorResponse?>
    ): JsonObjectRequest? {
        when (query) {
            "invalid_credentials" -> isError.value = InvalidCredentialResponse()
            "nothing" -> isError.value = NoResultsResponse()
            else -> isError.value = ErrorResponse(
                "As expected, the search has failed, and this is the body of the error.",
                "418")
        }
        return null
    }
}