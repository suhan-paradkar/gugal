# Contributing to Gugal
First, thank you for deciding to contribute to Gugal. This app is essentially made by only one person, so your contributions help a lot.

## About issues in this project
Gugal is mostly managed using the Issues tab, as to my knowledge there are no forum/project/etc. features on GitLab. You can suggest improvements or report bugs there, and Gugal maintainers frequently file upcoming and necessary improvements as issues.

## How can I contribute?
There are multiple ways in which you can contribute to Gugal.

### Translations
If you want to translate Gugal into another language or clean up or fix errors in an existing translation check out our [Weblate](https://hosted.weblate.org/engage/gugal).

### Support for search engines
To search the web Gugal uses SERP providers. You can find out more [on our site](https://gugal.gitlab.io/serp).

### Design
You can check for any open issues with the `design` tag [here](https://gitlab.com/narektor/gugal/-/issues/?state=opened&label_name[]=design). These are about parts of Gugal that need a redesign or design improvements. Make a reply and attach your suggested design as a file.
You can also make an issue with the `design` tag and attach your suggested design as a design file. If you are also familiar with Android development you can instead make a pull request with that tag and an implementation of your design.

### Code
Gugal is built with Jetpack Compose and Kotlin. The dependencies can be found in [the development guide](https://gitlab.com/gugal/gugal.gitlab.io/-/blob/master/serp/creation.md#practice).
Don't know where to start? [Beginner issues](https://gitlab.com/narektor/gugal/-/issues/?state=opened&label_name%5B%5D=beginner) are smaller changes that only need a few lines of code to implement.

### Documentation
Like Gugal, it's documentation [is also open source](https://gitlab.com/gugal/gugal.gitlab.io). That project follows the same contribution guidelines as Gugal itself.
From a technical standpoint Gugal documentation is written in Markdown, and compiled to HTML using GitBook.
Here are some best practices for editing documentation:
- **Don't load images or code from 3rd party sources**, unless absolutely necessary.
- **Don't advertise your social media/blog/similar.** This isn't an advertisement board after all.
    - Similarly, **don't add ads or analytics** to Gugal documentation.
    - If you want to be credited, add a credit to the end of the page, like this:
    > [Your username](https://gitlab.com/gugal) contributed to this page.
- If documenting **in-development** (i.e. the `main` branch) **and preview versions of Gugal add the link to the [development documentation](https://gitlab.com/gugal/gugal.gitlab.io/-/blob/master/dev-docs.md)** as well as the sidebar.
> Note: in this document, "in-development version" refers to what's on the `main` branch and not yet available in any released version, while "preview version" refers to early versions available [here](https://gugal.gitlab.io/preview.html).
- If contributing code to Gugal you can document it and make a merge request to the documentation repo when you make the code merge request to Gugal or after it gets merged, **but not before the code merge request.**
    - It would be nice if you **link the code merge request** in the documentation one, and the documentation merge request in the code one.
    - **Documentation MRs without corresponding code MRs will be closed.** Documentation needs to apply to versions of Gugal.
        - If you want to suggest improvements read "Suggesting improvements" below.

## Reporting bugs
Found an issue in Gugal? Here's how to report it.
 
### Before you report
- Try to reproduce the issue with the latest version of Gugal.
- If possible, try to reproduce with another search engine (e.g. if you were using Google when the issue occured, try searx).
- Make sure you are connected to the Internet.
- Check the [Issues tab](https://gitlab.com/narektor/gugal/-/issues) to see if the issue has already been reported.

### Reporting
When reporting an issue, make sure to follow these best practices. This helps maintainers fix the issue.

- **Use a clear and descriptive title.**
- **Include the version of Gugal and the search engine.**
    - You can find the version number by going to the Apps setting (or a similar one) and selecting Gugal.
    - Some Android skins (e.g. Google Pixel) allow you to copy the version number by long pressing it, but some (e.g. ColorOS) don't.
    - If reporting an issue that occurs on an in-development version (i.e. the `main` branch) and not a specific release, include the latest commit's URL or hash.
- **Include your device's model and Android version.**
    - These can be found in About phone or a similar setting.
    - If a model number is shown (like in One UI, e.g. SM-G991B), include it instead of the model name (e.g. Galaxy S21 5G).
    - Please include the build number (e.g. TP1A.220905.004).
        - This can be found in the Android version setting in About phone (or a similarly named setting).
        - Some Android skins (e.g. Google Pixel) allow you to copy the build number by long pressing it, but some don't. If your skin doesn't support copying the build number you can include the security patch level (usually in the same setting) instead.
- **Include your system language.** This is especially important if you're reporting translation and search issues.
- **Attach screenshots** and, if possible, screen recordings.
- **DO NOT include sensitive credentials** (API keys, instance URLs).
    - These credentials should be kept private.
- **Provide specific examples and the exact steps** which reproduce the problem.
    - If the issue is triggered by a search query include it if possible.
- **Describe the behavior you see** and point out the problem.
- **Describe what you expected to see**.
- **Please use our issue templates**. These have fields for all of the necessary information.

## Suggesting improvements
Improvements are also suggested as issues. Here are some best practices:
- **Add the `improvement` tag** to your issue.
- **Search for similar improvements** [here](https://gitlab.com/narektor/gugal/-/issues/?state=opened&label_name[]=improvement) before suggesting. If your improvement was already reported, add a comment.
- If possible **add design mockups**.
- **Suggest other search apps and websites** where the enhancement exists. These can be for web search (e.g. Google, DuckDuckGo), local search (e.g. S Finder on Samsung devices) or just the search function of some app.
- **Explain why it would be useful** to other Gugal users.

## Making good merge requests
Merge requests are the main way to contribute code to Gugal. Here are some best practices:
- **Use our tags** to organize merge requests.
- If this closes an issue, **mention the issue** in the merge request.
- For commit descriptions follow this simple convention:
    - If changing only a part of Gugal, such as a SERP provider, setup wizard or the About page, **prefix the commit name with that part's name** (e.g. [SearX SERP: add no credential response ](https://gitlab.com/narektor/gugal/-/commit/37a8e48c6de770ee8d826c44858426bcaa78de22)).
    - Similarly, if changing a resource document, like this guide or the readme, or Gugal's documentation, **prefix the commit name with the document's name** (e.g. [README: we aren't Google only anymore](https://gitlab.com/narektor/gugal/-/commit/24ef33028c306ec522f924081b7bfe93912656c6)).
