# Gugal <img src="fastlane/metadata/android/en-US/images/icon.png" width="30px" alt="Icon"/>

A clean, lightweight, FOSS web search app.

<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.png" width="200px" alt="Main screenshot"/> <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.png" width="200px" alt="Setup wizard"/> <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/3.png" width="200px" alt="Search engine selection"/>

## Download

Gugal can be downloaded from [the GitLab releases page](https://gitlab.com/narektor/gugal/-/releases), and is also available on F-Droid [here](https://f-droid.org/packages/com.porg.gugal/).

Download from one _or_ the other. The versions are incompatible as F-Droid signs apps themselves.

### 0.2 and 0.3

0.2 and 0.3 were removed to prevent F-Droid from releasing them as updates to newer versions. The APKs and source code can be found [here](https://gitlab.com/narektor/gugal-rel/-/tree/main/deleted-tags).

## Set up

To use Gugal with Google Search you need a CSE (Programmable Search Engine) ID and API key. Instructions for obtaining both can be found in the app.

**To get these values you must have a Google account, therefore Google might still link searches to your account.**

To use Gugal with searx or SearXNG you need an instance **with API access**. We recommend self-hosting searx as most public instances don't have it enabled.

## Contributing

### Translations
If you want to translate Gugal into your language or clean up an existing translation, check out our [Weblate](https://hosted.weblate.org/engage/gugal).

### SERP providers
See the [development guide](https://gugal.gitlab.io/serp/creation.html).

### Code
Gugal is built with Jetpack Compose and Kotlin.
