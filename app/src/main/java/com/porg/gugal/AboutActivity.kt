/*
 *     AboutActivity.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import com.porg.gugal.ui.theme.GugalTheme
import java.util.*

class AboutActivity : ComponentActivity() {

    val PREVIEW_ISSUE_URL: String = "https://gitlab.com/narektor/gugal/-/issues/new?issue[description]=/label%20~beta%20%3C!--%20write%20below%20this%20line%20--%3E"

    @ExperimentalAnimationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val context = this
        val currentLocale: Locale = resources.configuration.locale
        setContent {
            GugalTheme {
                Surface(color = MaterialTheme.colorScheme.background) {
                    Column(
                        modifier = Modifier.verticalScroll(rememberScrollState())
                    ) {
                        IconButton(
                            onClick = { finish() },
                            modifier = Modifier.padding(
                                start = 16.dp,
                                top = 16.dp,
                                bottom = 0.dp,
                                end = 16.dp
                            )
                        ) {
                            Icon(
                                imageVector = Icons.Filled.ArrowBack,
                                contentDescription = "Go back",
                            )
                        }
                        // Gugal logo
                        Column(modifier = Modifier.fillMaxSize()) {
                            Card(
                                modifier = Modifier
                                    .padding(all = 24.dp)
                                    .size(128.dp)
                                    .align(alignment = Alignment.CenterHorizontally),
                                shape = CircleShape,
                                backgroundColor = colorResource(R.color.icon_bg)
                            ) {
                                Image(
                                    painterResource(R.drawable.ic_launcher_foreground),
                                    contentDescription = "",
                                    contentScale = ContentScale.Crop,
                                    modifier = Modifier.fillMaxSize()
                                )
                            }
                            Text(
                                text = "Gugal " + getVersion(),
                                modifier = Modifier.fillMaxWidth(),
                                textAlign = TextAlign.Center,
                                style = MaterialTheme.typography.titleLarge
                            )
                        }
                        if (BuildConfig.VERSION_NAME.contains(".p")) {
                            Material3SetupWizard.Tip(
                                text = getString(R.string.tip_preview),
                                modifier = Modifier.padding(top = 24.dp, start = 16.dp, end = 16.dp), icon = R.drawable.ic_info,
                                onClick = { open(context, PREVIEW_ISSUE_URL) }
                            )
                        }
                        Material3Settings.RegularSetting(
                            R.string.setting_donate_title,
                            R.string.setting_donate_desc,
                            onClick = { open(context, "https://ko-fi.com/thegreatporg") }
                        )
                        Material3Settings.RegularSetting(
                            R.string.setting_issue_title,
                            R.string.setting_issue_desc,
                            onClick = { open(context, "https://gitlab.com/narektor/gugal/-/issues/new") }
                        )
                        Material3Settings.RegularSetting(
                            R.string.setting_gitlab_title,
                            R.string.setting_gitlab_desc,
                            onClick = { open(context, "https://gitlab.com/narektor/gugal") }
                        )
                        Material3Settings.RegularSetting(
                            R.string.setting_changelog_title,
                            R.string.setting_changelog_desc,
                            onClick = {
                                val intent = Intent(context, ChangelogActivity::class.java)
                                intent.component =
                                    ComponentName(
                                        "com.porg.gugal",
                                        "com.porg.gugal.ChangelogActivity"
                                    )
                                ContextCompat.startActivity(context, intent, null)
                            }
                        )
                        Material3Settings.RegularSetting(
                            R.string.setting_translate_title,
                            R.string.setting_translate_desc,
                            onClick = { open(context, "https://hosted.weblate.org/engage/gugal/") }
                        )
                        Divider(
                            modifier = Modifier
                                .fillMaxWidth()
                                .width(1.dp)
                                .padding(start = 16.dp, end = 16.dp)
                        )
                        Text(
                            text = getString(R.string.about_contributors_start),
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(top = 24.dp),
                            textAlign = TextAlign.Center,
                            style = MaterialTheme.typography.bodyMedium
                        )

                        Contributor(
                            context,
                            "narektor",
                            if (currentLocale.language == "ru") getString(R.string.role_developer_trans)
                            else getString(R.string.role_developer),
                            "https://gitlab.com/narektor",
                            R.drawable.cont_narektor
                        )
                        Contributor(
                            context,
                            "iSteven",
                            getString(R.string.role_designer),
                            "https://t.me/OneSteven",
                            R.drawable.cont_steven
                        )
                        ContributorForLocale(currentLocale, context)
                    }
                }
            }
        }
    }

    @Composable
    fun ContributorForLocale(locale: Locale, context: Context) {
        // Find the language code at https://www.iana.org/assignments/language-subtag-registry/language-subtag-registry,
        // it's usually written after "Subtag:"
        when (locale.language) {
            // Languages with >=75% translated
            "ko" -> {
                Contributor(
                    context,
                    "Junghee",
                    getString(R.string.role_translator),
                    "https://gitlab.com/Junghee_Lee",
                    R.drawable.cont_junghee
                )
            }
            "tr" -> {
                Contributor(
                    context,
                    "metezd",
                    getString(R.string.role_translator),
                    "mailto:itoldyouthat@protonmail.com",
                    R.drawable.cont_metezd
                )
                Contributor(
                    context,
                    "Oğuz Ersen",
                    getString(R.string.role_translator),
                    "https://ersen.moe/",
                    R.drawable.cont_ersen
                )
            }
            // Languages with >=15% translated
            "nb" -> {
                Contributor(
                    context,
                    "Allan Nordhøy",
                    getString(R.string.role_translator),
                    "https://gitlab.com/kingu",
                    R.drawable.cont_kingu
                )
            }
            "fr" -> {
                Contributor(
                    context,
                    "J. Lavoie",
                    getString(R.string.role_translator),
                    "mailto:j.lavoie@net-c.ca",
                    R.drawable.cont_lavoie
                )
            }
            // Please don't add contributors for languages with less than 15% translated.
        }

        // Special case for Simplified Chinese (zh-Hans)
        if (locale.language == "zh" && locale.script == "Hans") {
            Contributor(
                context,
                "Hugel",
                getString(R.string.role_translator),
                "mailto:qihu@nfschina.com",
                R.drawable.cont_hugel
            )
        }
    }

    // A Material3Settings-like component displaying information about a contributor.
    @Composable
    fun Contributor(context: Context, name: String, role: String, url: String, picture: Int) {
        Row(modifier = Modifier
            .clickable { open(context, url) }
            .fillMaxWidth()
            .padding(start = Material3Settings.padding),
            verticalAlignment = Alignment.CenterVertically) {
            Image(
                painter = painterResource(picture),
                contentDescription = "$name's profile picture",
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .size(48.dp)
                    .clip(CircleShape)
            )
            Column(modifier = Modifier.padding(start = 15.dp)) {
                Text(
                    text = name,
                    modifier = Modifier.padding(end = Material3Settings.padding, top = Material3Settings.padding, bottom = Material3Settings.dividerPadding),
                    style = MaterialTheme.typography.titleLarge
                )
                Text(
                    text = role,
                    modifier = Modifier.padding(end = Material3Settings.padding, bottom = Material3Settings.padding),
                    style = MaterialTheme.typography.bodyMedium
                )
            }
        }
    }

    private fun open(context: Context, url: String) {
        val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse(url)
        )
        ContextCompat.startActivity(
            context,
            Intent.createChooser(intent, getString(R.string.open_in)),
            null
        )
    }

    private fun getVersion(): String {
        val split = BuildConfig.VERSION_NAME.split(".p")
        if (split.size == 2) {
            return "${split[0]} preview ${split[1]}"
        }
        return BuildConfig.VERSION_NAME
    }
}