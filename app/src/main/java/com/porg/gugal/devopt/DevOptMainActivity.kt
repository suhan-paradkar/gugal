/*
 *     DevOptMainActivity.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.devopt

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import com.porg.gugal.Material3Settings
import com.porg.gugal.Material3SetupWizard
import com.porg.gugal.R
import com.porg.gugal.setup.SetupStartActivity
import com.porg.gugal.ui.theme.GugalTheme

class DevOptMainActivity : ComponentActivity() {
    @OptIn(
        ExperimentalMaterialApi::class,
        androidx.compose.animation.ExperimentalAnimationApi::class,
        androidx.compose.material3.ExperimentalMaterial3Api::class
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            GugalTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colorScheme.background) {
                    Column(
                        modifier = Modifier.fillMaxSize()
                    ) {
                        Material3SetupWizard.Tip(
                            text = "Be careful! The settings here are intended for development only and might lower security or break Gugal.",
                            modifier = Material3SetupWizard.PaddingModifier,
                            R.drawable.ic_warning
                        )
                        Material3Settings.RegularSetting(
                            title = "View encrypted preferences",
                            body = "Shows all encrypted preferences that Gugal stores on this device. SERP provider sensitive credentials are shown partially.",
                            onClick = {
                                val intent = Intent(applicationContext, DevOptSecurePrefsViewer::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                startActivity(intent)
                            }
                        )
                        Material3Settings.RegularSetting(
                            title = "Restart setup",
                            body = "Launches the setup start activity again, without removing preferences.",
                            onClick = {
                                val intent = Intent(applicationContext, SetupStartActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                startActivity(intent)
                            }
                        )
                        Material3Settings.RegularSetting(
                            title = "Development SERP providers",
                            body = "Shows a list of development SERP providers.",
                            onClick = {
                                LaunchSetupWithDevProviders()
                            }
                        )

                        Material3Settings.RegularSetting(
                            title = "Experiments",
                            body = "Shows a list of experimental features, which might come in later versions of Gugal.",
                            onClick = {
                                startActivity(Intent(applicationContext, DevOptExperimentActivity::class.java))
                            },
                        )
                    }
                }
            }
        }
    }

    private fun LaunchSetupWithDevProviders() {
        val intent = Intent(applicationContext, DevOptSelectSerpActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
}