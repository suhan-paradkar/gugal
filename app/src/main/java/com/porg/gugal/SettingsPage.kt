/*
 *     SettingsPage.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat.startActivity
import com.porg.gugal.Material3Settings.Companion.RegularSetting

@Composable
fun SettingsPage(context: Context?) {
    Column(
        modifier = Modifier.verticalScroll(rememberScrollState())
    ) {
        val openClearDialog = remember { mutableStateOf(false) }

        RegularSetting(
            R.string.setting_about_title,
            R.string.setting_about_desc,
            onClick = {
                val intent = Intent(context, AboutActivity::class.java)
                intent.component =
                    ComponentName("com.porg.gugal", "com.porg.gugal.AboutActivity")
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(context!!, intent, null)
            })
        RegularSetting(
            R.string.setting_cred_title,
            R.string.setting_cred_desc,
            onClick = {
                val intent = Intent(context, ChangelogActivity::class.java)
                intent.component =
                    ComponentName("com.porg.gugal", "com.porg.gugal.setup.SetupConfigureSerpActivity")
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("launchedFromSettings", true)
                startActivity(context!!, intent, null)
            })
        RegularSetting(
            R.string.setting_serp_title,
            R.string.setting_serp_desc,
            onClick = {
                val intent = Intent(context, ChangelogActivity::class.java)
                intent.component =
                    ComponentName("com.porg.gugal", "com.porg.gugal.setup.SetupSelectSerpActivity")
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("launchedFromSettings", true)
                startActivity(context!!, intent, null)
            })
        if (Global.pastSearches.isNotEmpty())
            RegularSetting(
                stringResource(R.string.setting_clearHistory_title),
                stringResource(R.string.setting_clearHistory_desc),
                onClick = { openClearDialog.value = true })

        if (openClearDialog.value) {
            AlertDialog(
                onDismissRequest = {},
                title = {Text(text = stringResource(R.string.dialog_confirm))},
                text = {Text(text = stringResource(R.string.dialog_clearHistory))},
                confirmButton = {
                    TextButton(
                        onClick = {
                            // Clear search history in prefs
                            with (Global.sharedPreferences.edit()) {
                                remove("pastSearches")
                                apply()
                            }
                            // Clear local copy
                            Global.pastSearches = mutableSetOf()
                            // Close dialog
                            openClearDialog.value = false
                        }
                    ) {Text(stringResource(R.string.btn_yes))}
                },
                dismissButton = {
                    TextButton(
                        onClick = {openClearDialog.value = false}
                    ) {Text(stringResource(R.string.btn_no))}
                }
            )
        }

        Text(
            text = "Gugal " + BuildConfig.VERSION_NAME,
            modifier = Modifier.padding(start = 20.dp),
            style = MaterialTheme.typography.bodyMedium
        )
    }
}

fun clearHistory() {

}
