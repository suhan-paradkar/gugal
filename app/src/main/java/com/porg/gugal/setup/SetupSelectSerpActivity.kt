/*
 *     SetupSelectSerpActivity.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.setup

import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.text.style.TextOverflow
import com.porg.gugal.BuildConfig
import com.porg.gugal.Global
import com.porg.gugal.Global.Companion.setSerpProvider
import com.porg.gugal.Global.Companion.sharedPreferences
import com.porg.gugal.Material3Settings.Companion.RadioSetting
import com.porg.gugal.Material3SetupWizard
import com.porg.gugal.R
import com.porg.gugal.providers.ProviderInfo
import com.porg.gugal.providers.cse.GoogleCseSerp
import com.porg.gugal.providers.searx.SearXSerp
import com.porg.gugal.ui.theme.GugalTheme

class SetupSelectSerpActivity : ComponentActivity() {
    @OptIn(
        ExperimentalMaterialApi::class,
        androidx.compose.animation.ExperimentalAnimationApi::class,
        androidx.compose.material3.ExperimentalMaterial3Api::class
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val currentSerpProviderId = mutableStateOf("")
        val ctx = this
        setContent {

            val scrollBehavior = TopAppBarDefaults.exitUntilCollapsedScrollBehavior()

            GugalTheme {
                val _csp_id by currentSerpProviderId

                Scaffold(
                    modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
                    topBar = {
                        LargeTopAppBar(
                            title = {
                                Text(
                                    getText(R.string.setup_p2_title).toString(),
                                    maxLines = 1,
                                    overflow = TextOverflow.Ellipsis
                                )
                            },
                            navigationIcon = {
                                IconButton(onClick = { finish() }) {
                                    Icon(
                                        imageVector = Icons.Filled.ArrowBack,
                                        contentDescription = "Go back",
                                    )
                                }
                            },
                            scrollBehavior = scrollBehavior
                        )
                    },
                    content = { innerPadding ->
                        Surface(color = MaterialTheme.colorScheme.background) {
                            Column(
                                modifier = Modifier.padding(innerPadding)
                                    .verticalScroll(rememberScrollState()).fillMaxSize()
                            ) {
                                Global.allSerpProviders.forEach { serpID ->
                                    if (_csp_id == "") {
                                        currentSerpProviderId.value = serpID
                                    }
                                    val info: ProviderInfo = getInfo(serpID)!!
                                    RadioSetting(
                                        title = info.name,
                                        body = info.description,
                                        selected = _csp_id == serpID,
                                        onClick = {
                                            currentSerpProviderId.value = serpID
                                        }
                                    )
                                }
                            }
                        }
                        Material3SetupWizard.TwoButtons(
                            positiveAction = {
                                setSerpProvider(_csp_id)
                                val info = getInfo(_csp_id)

                                var nextActivity = "SetupFOSSActivity"
                                if (info?.requiresSetup!!) nextActivity = "SetupConfigureSerpActivity"
                                saveSerp(_csp_id, info.requiresSetup)

                                val intent = Intent(ctx, SetupConfigureSerpActivity::class.java)
                                intent.component =
                                    ComponentName("com.porg.gugal", "com.porg.gugal.setup.$nextActivity")
                                startActivity(intent)
                            },
                            positiveText = getString(R.string.btn_next),
                            negativeAction = {
                                finish()
                            }
                        )
                    }
                )
            }
        }
    }

    // TODO if adding SERP providers, add your provider here
    private fun getInfo(serpID: String): ProviderInfo? {
        if (serpID == GoogleCseSerp.id) return GoogleCseSerp.providerInfo
        else if (serpID == SearXSerp.id) return SearXSerp.providerInfo
        // Check if serpID matches your SERP provider's ID, and if so return your SERP provider's
        // provider info.
        return null
    }

    fun saveSerp(serpID: String, saveGugalVersion: Boolean = false) {
        with (sharedPreferences.edit()) {
            this.putString("serp", serpID)
            // Save the last Gugal version to not show the changelog again
            if (saveGugalVersion) this.putInt("lastGugalVersion", BuildConfig.VERSION_CODE)
            apply()
        }
    }
}