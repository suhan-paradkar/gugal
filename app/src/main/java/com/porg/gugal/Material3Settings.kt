/*
 *     Material3Settings.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

class Material3Settings {
    companion object {

        val padding = 20.dp
        val dividerPadding = 5.dp

        @Composable
        fun RegularSetting(title: Int, body: Int, onClick: (() -> Unit)) {
            RegularSetting(
                stringResource(title),
                stringResource(body),
                onClick
            )
        }

        @Composable
        fun RegularSetting(title: String, body: String, onClick: (() -> Unit)) {
            Column(
                modifier = Modifier.clickable { onClick() }.fillMaxWidth(),
            ) {
                Text(
                    text = title,
                    modifier = Modifier.padding(start = padding, end = padding, top = padding, bottom = dividerPadding),
                    style = MaterialTheme.typography.titleLarge
                )
                Text(
                    text = body,
                    modifier = Modifier.padding(start = padding, end = padding, bottom = padding),
                    style = MaterialTheme.typography.bodyMedium
                )
            }
        }


        @OptIn(ExperimentalMaterial3Api::class)
        @Composable
        fun RadioSetting(title: Int, body: Int, onClick: (() -> Unit), selected: Boolean) {
            RadioSetting(
                stringResource(title),
                stringResource(body),
                onClick,
                selected
            )
        }

        @OptIn(ExperimentalMaterial3Api::class)
        @Composable
        fun RadioSetting(title: String, body: String, onClick: (() -> Unit), selected: Boolean) {
            Row(modifier = Modifier.clickable { onClick() }.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically) {
                RadioButton(selected = selected,
                    onClick = onClick,
                    modifier = Modifier.padding(start = padding, end = padding)
                )
                Column {
                    Text(
                        text = title,
                        modifier = Modifier.padding(end = padding, top = padding, bottom = dividerPadding),
                        style = MaterialTheme.typography.titleLarge
                    )
                    Text(
                        text = body,
                        modifier = Modifier.padding(end = padding, bottom = padding),
                        style = MaterialTheme.typography.bodyMedium
                    )
                }
            }
        }


        @OptIn(ExperimentalMaterial3Api::class)
        @Composable
        fun ToggleSetting(title: Int, body: Int, onCheckedChange: ((Boolean) -> Unit), selected: Boolean) {
            ToggleSetting(
                stringResource(title),
                stringResource(body),
                onCheckedChange,
                selected
            )
        }

        @OptIn(ExperimentalMaterial3Api::class)
        @Composable
        fun ToggleSetting(title: String, body: String, onCheckedChange: ((Boolean) -> Unit), selected: Boolean) {
            Row(
                modifier = Modifier.clickable { onCheckedChange(!selected) }
                    .fillMaxWidth()
                    .padding(start = padding, end = padding),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement  =  Arrangement.SpaceBetween
            ) {
                Column () {
                    Text(
                        text = title,
                        modifier = Modifier.padding(top = padding, bottom = dividerPadding),
                        style = MaterialTheme.typography.titleLarge
                    )
                    Text(
                        text = body,
                        modifier = Modifier.padding(bottom = padding),
                        style = MaterialTheme.typography.bodyMedium
                    )
                }
                Switch(
                    checked = selected,
                    onCheckedChange = onCheckedChange,
                )
            }
        }

        @Preview
        @Composable
        private fun RegularSettingPreview() {
            Surface() {
                RegularSetting(R.string.setting_about_title, R.string.setting_about_desc) {}
            }
        }
    }
}