/*
*     ResultsPage.kt
*     Gugal
*     Copyright (c) 2022 thegreatporg
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package com.porg.gugal

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.content.SharedPreferences
import android.net.Uri
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.PressInteraction
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat.startActivity
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.porg.gugal.Global.Companion.pastSearches
import com.porg.gugal.Global.Companion.serpProvider
import com.porg.gugal.Global.Companion.sharedPreferences
import com.porg.gugal.devopt.DevOptSecurePrefsViewer
import com.porg.gugal.providers.responses.ErrorResponse
import com.porg.gugal.providers.responses.InvalidCredentialResponse
import com.porg.gugal.setup.SetupConfigureSerpActivity
import com.porg.gugal.setup.SetupStartActivity
import com.porg.gugal.ui.theme.GugalTheme
import com.porg.gugal.ui.theme.shapeScheme
import kotlinx.coroutines.delay
import kotlin.time.Duration.Companion.seconds

val cardElevation = 15.dp

@OptIn(ExperimentalMaterial3Api::class)
@ExperimentalAnimationApi
@Composable
fun ResultPage(context: Context, noCredentials: Boolean) {
    val queue: RequestQueue = Volley.newRequestQueue(context)
    var startIntent: Intent? = null
    // the changelog shouldn't be shown on first run
    if (noCredentials && serpProvider.providerInfo?.requiresSetup ?: noCredentials) {
        startIntent = Intent(context, SetupStartActivity::class.java)
        startIntent.component =
            ComponentName("com.porg.gugal", "com.porg.gugal.setup.SetupStartActivity")
    } else if (shouldShowChangelog(context)) {
        startIntent = Intent(context, ChangelogActivity::class.java)
        startIntent.component =
            ComponentName("com.porg.gugal", "com.porg.gugal.ChangelogActivity")
    }

    if (startIntent != null) {
        startIntent.flags = FLAG_ACTIVITY_NEW_TASK
        startActivity(context, startIntent, null)
    }

    val textState = remember { mutableStateOf("") }
    Column {
        val res = remember { mutableStateListOf<Result>() }
        val error = remember { mutableStateOf<ErrorResponse?>(null) }
        val errorResponse by error

        // top card
        val showLogo = remember { mutableStateOf(true) }
        Box(modifier = Modifier
            .animateContentSize()
            .fillMaxWidth()) {
            if (showLogo.value) {
                androidx.compose.material.Card(
                    modifier = Modifier
                        .padding(all = 48.dp)
                        .size(96.dp)
                        .align(alignment = Alignment.Center),
                    shape = CircleShape,
                    backgroundColor = colorResource(R.color.icon_bg)
                ) {
                    Image(
                        painterResource(R.drawable.ic_launcher_foreground),
                        contentDescription = "",
                        contentScale = ContentScale.Crop,
                        modifier = Modifier.fillMaxSize()
                    )
                }
            }
        }

        // Automatically handle some error responses
        if (errorResponse != null) {
            when (errorResponse) {
                // If an InvalidCredentialResponse is received, open the setup page
                is InvalidCredentialResponse -> {
                    val intent = Intent(context, SetupConfigureSerpActivity::class.java)
                    intent.putExtra("launchedFromSettings", true)
                    startActivity(context, intent, null)
                    error.value = null
                }
            }
        }

        val tfSidePadding = if (showLogo.value) 14.dp else 4.dp
        val tfFocusRequester = remember { FocusRequester() }

        TextField(
            placeholder = {
                Text(text = stringResource(R.string.sb_message)
                    .format(stringResource(serpProvider.providerInfo!!.titleInSearchBox)))
            },
            value = textState.value,
            shape = RoundedCornerShape(60.dp),
            colors = TextFieldDefaults.textFieldColors(
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent
            ),
            interactionSource = remember { MutableInteractionSource() }
                .also { interactionSource ->
                    LaunchedEffect(interactionSource) {
                        interactionSource.interactions.collect {
                            if (it is PressInteraction.Release) {
                                showLogo.value = false
                            }
                        }
                    }
                },
            modifier = Modifier
                .padding(
                    top = 4.dp, bottom = 4.dp,
                    start = tfSidePadding, end = tfSidePadding
                )
                .fillMaxWidth()
                .focusRequester(tfFocusRequester),
            onValueChange = { nv ->
                textState.value = nv
            },
            keyboardActions = KeyboardActions(
                onSearch = {
                    val tsvt = textState.value
                    if (tsvt == "_gugal_devopts") {
                        val intent = Intent(context, DevOptSecurePrefsViewer::class.java)
                        intent.component =
                            ComponentName("com.porg.gugal", "com.porg.gugal.devopt.DevOptMainActivity")
                        intent.flags = FLAG_ACTIVITY_NEW_TASK
                        context.startActivity(intent)
                    } else {
                        saveSearch(tsvt)
                        // Clear error response
                        error.value = null
                        // Search using the current SERP provider
                        val req = serpProvider.search(tsvt, res, error)
                        if (req != null) queue.add(req)
                    }
                }
            ),
            maxLines = 1,
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Search
            )
        )

        // results card
        Box(modifier = Modifier.fillMaxWidth()) {
            if (showLogo.value) {
               SearchHistory(context, onClick = { query ->
                   showLogo.value = false
                   textState.value = query
                   // Search using the current SERP provider
                   val req = serpProvider.search(query, res, error)
                   if (req != null) queue.add(req)
               }, fillOnClick = { query ->
                   showLogo.value = false
                   textState.value = query
                   // Focus on result field
                   tfFocusRequester.requestFocus()
               })
            }
        }

        if (errorResponse == null) {
            Results(
                results = res,
                context
            )
        } else {
            if (errorResponse!!.errorCode == "_NRR") ErrorMessage("")
            else ErrorMessage(errorResponse!!.body)
        }
    }
}

fun saveSearch(tsvt: String) {
    pastSearches = ((mutableSetOf(tsvt) + pastSearches) as MutableSet<String>)
    with (sharedPreferences.edit()) {
        this.putStringSet("pastSearches", pastSearches)
        apply()
    }
}

@Composable
fun ErrorMessage(body: String) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        if (body.isNotEmpty()) {
            Text(
                stringResource(R.string.rp_error),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.headlineSmall
            )
            Text(
                body,
                modifier = Modifier
                    .padding(all = 10.dp)
                    .fillMaxWidth(),
                textAlign = TextAlign.Center
            )
        } else {
            Text(
                stringResource(R.string.rp_noresult),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.headlineSmall
            )
        }
    }
}

fun shouldShowChangelog(context: Context): Boolean {
    try {
        // Although you can define your own key generation parameter specification, it's
        // recommended that you use the value specified here.
        val keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC
        val mainKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec)

        val sharedPrefsFile = "gugalprefs"
        val sharedPreferences: SharedPreferences = EncryptedSharedPreferences.create(
            sharedPrefsFile,
            mainKeyAlias,
            context,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )

        // if there is no lastGugalVersion preference the version is older than 0.5
        if (!sharedPreferences.contains("lastGugalVersion")) {
            with(sharedPreferences.edit()) {
                this.putInt("lastGugalVersion", BuildConfig.VERSION_CODE)
                apply()
                return true
            }
        }

        if (sharedPreferences.getInt("lastGugalVersion",
                BuildConfig.VERSION_CODE) < BuildConfig.VERSION_CODE
        ) {
            with(sharedPreferences.edit()) {
                this.putInt("lastGugalVersion", BuildConfig.VERSION_CODE)
                apply()
                return true
            }
        } else return false
    }
    // if any errors occur reading/writing to the preferences, don't show the changelog
    catch (exc: Exception) {
        return false
    }
}

@Composable
fun ResultCard(res: Result, context: Context?) {
    Surface(
        shape = MaterialTheme.shapeScheme.medium,
        tonalElevation = cardElevation,
        modifier = Modifier
            .padding(all = 4.dp)
            .fillMaxWidth()
            .clickable(onClick = {
                if (res.url != "") {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(res.url))
                    // Note the Chooser below. If no applications match,
                    // Android displays a system message.So here there is no need for try-catch.
                    startActivity(context!!, Intent.createChooser(intent, "Open result in"), null)
                }
            }),
    ) {
        Column {
            Text(
                text = res.title,
                modifier = Modifier.padding(all = 4.dp),
                style = MaterialTheme.typography.titleLarge
            )
            if (res.domain != "") {
                Text(
                    text = res.domain,
                    modifier = Modifier.padding(top = 2.dp, bottom = 2.dp, start = 4.dp, end = 4.dp),
                    style = MaterialTheme.typography.titleSmall
                )
            }
            if (res.body != null) {
                // Add a vertical space between the author and message texts
                Spacer(modifier = Modifier.height(2.dp))
                Text(
                    text = res.body,
                    modifier = Modifier.padding(all = 4.dp),
                    style = MaterialTheme.typography.bodyLarge
                )
            }
        }
    }
}

@Composable
@Preview
fun PreviewResultCard() {
    GugalTheme {
        ResultCard(
            res = Result("Colleague", "Hey, take a look at Jetpack Compose, it's great!", "about:blank", "test.com"),
            context = null
        )
    }
}

@ExperimentalAnimationApi
@Composable
fun Results(results: List<Result>, applicationContext: Context?) {
    LazyColumn {
        items(results) { message ->
            if (Global.resAppAnim) {
                var visible by remember { mutableStateOf(false) }
                AnimatedVisibility(visible) {
                    ResultCard(message, applicationContext)
                }
                LaunchedEffect(Unit) {
                    delay(0.1.seconds)
                    visible = true
                }
            } else ResultCard(message, applicationContext)
        }
    }
}

@ExperimentalAnimationApi
@Composable
fun SearchHistory(applicationContext: Context?, onClick: (String) -> Unit, fillOnClick: (String) -> Unit) {
    Surface(
        shape = MaterialTheme.shapeScheme.large,
        tonalElevation = cardElevation,
        modifier = Modifier
            .padding(start = 14.dp, end = 14.dp, top = 24.dp, bottom = 24.dp)
            .fillMaxWidth()
            .fillMaxHeight()
    ) {
        LazyColumn {
            items(pastSearches.toList()) { message ->
                PastSearch(message, onClick = {onClick(message)}, fillOnClick = {fillOnClick(message)})
            }
        }
    }
}

@Composable
fun PastSearch(query: String, onClick: () -> Unit, fillOnClick: () -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable(onClick = onClick)
            .padding(top = 5.dp, bottom = 5.dp, start = 20.dp, end = 5.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = query,
            style = MaterialTheme.typography.titleLarge
        )
        IconButton(onClick = fillOnClick) {
            Icon(painterResource(id = R.drawable.ic_fill_result), stringResource(R.string.btn_fill_result))
        }
    }
}

@ExperimentalAnimationApi
@Composable
@Preview
fun ResultsPreview() {
    GugalTheme {
        Results(
            results = List(size = 3) {
                Result(
                    "Google privacy scandal",
                    "Google have been fined for antitrust once again, a few days after people bought more than 5 Pixels.",
                    "about:blank",
                    "test.com"
                )
            },
            applicationContext = null
        )
    }
}