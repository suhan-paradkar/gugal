- 최초 사용 카드를 대체하는 Material 3 설정 마법사가 추가되었습니다.
- Material 3/당신은 이제 더 많은 곳에서 사용됩니다.
- 설정 페이지가 추가되었습니다.
- 이전 자격 증명 업데이트에 대한 지원이 추가되었습니다.
- 결과에 스니펫이 없을 때 ANR을 수정했습니다.
